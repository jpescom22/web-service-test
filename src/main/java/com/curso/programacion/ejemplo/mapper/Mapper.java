package com.curso.programacion.ejemplo.mapper;

import org.springframework.stereotype.Component;

import com.curso.programacion.ejemplo.dto.SucursalDTO;
import com.curso.programacion.ejemplo.entity.SucursalEntity;

@Component
public class Mapper {

	public SucursalDTO mapSucursalEntityToSucursalDTO(SucursalEntity sucursalEntity) {
		final SucursalDTO response = new SucursalDTO();
		response.setDireccion(sucursalEntity.getDireccion());
		response.setFechaApertura(sucursalEntity.getFechaApertura());
		response.setId(sucursalEntity.getId());
		response.setNombre(sucursalEntity.getNombre());
		return response;
	}
	
	public SucursalEntity mapSucursalDTOToEntityInsert(SucursalDTO sucursalDTO) {
		final SucursalEntity response = new SucursalEntity();
		response.setDireccion(sucursalDTO.getDireccion());
		response.setFechaApertura(sucursalDTO.getFechaApertura());
		response.setNombre(sucursalDTO.getNombre());
		return response;
	}
	
	public SucursalEntity mapSucursalDTOToEntityUpdate(SucursalDTO sucursalDTO) {
		final SucursalEntity response = new SucursalEntity();
		response.setDireccion(sucursalDTO.getDireccion());
		response.setFechaApertura(sucursalDTO.getFechaApertura());
		response.setNombre(sucursalDTO.getNombre());
		response.setId(sucursalDTO.getId());
		return response;
	}
	
}
