package com.curso.programacion.ejemplo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.curso.programacion.ejemplo.entity.SucursalEntity;

@Repository
public interface SucursalRepository extends JpaRepository<SucursalEntity, Integer> {

}
