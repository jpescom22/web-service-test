package com.curso.programacion.ejemplo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.curso.programacion.ejemplo.entity.SucursalEmpleadoEntity;

@Repository
public interface SucursalEmpleadoRepository extends JpaRepository<SucursalEmpleadoEntity, Integer> {

}
