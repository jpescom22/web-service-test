package com.curso.programacion.ejemplo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.curso.programacion.ejemplo.dto.SucursalDTO;
import com.curso.programacion.ejemplo.service.ProgramacionEjemploService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping(value = "/curso-programacion/operacion")
public class ProgramacionEjemploController {
	
	private static final Logger log = LoggerFactory.getLogger(ProgramacionEjemploController.class);

	private final ObjectMapper mapper = new ObjectMapper();
	
		
	@Autowired
	private ProgramacionEjemploService programacionEjemploService;

	@CrossOrigin
	@PostMapping(value = "/consulta", produces = "application/json")
	public ResponseEntity<SucursalDTO> getSucursal(@RequestBody SucursalDTO request) throws JsonProcessingException {
		log.info("############ Retrieve Controller {} ", request);
		String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(request);
		log.info(":: JSON Request {} ", json);
		return new ResponseEntity<>(programacionEjemploService.getSucursal(request), HttpStatus.OK);
	}

	
	@PostMapping(value = "/actualizar", produces = "application/json")
	public ResponseEntity<SucursalDTO> updateSucursal(
			@RequestBody SucursalDTO request) throws JsonProcessingException {
		log.info(":: Update Controller {} ", request);
		String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(request);
		log.info(":: JSON Request {} ", json);
		return new ResponseEntity<>(programacionEjemploService.updateSucursal(request), HttpStatus.OK);
	}

	
	@PostMapping(value = "/insertar", produces = "application/json")
	public ResponseEntity<SucursalDTO> insertSucursal(@RequestBody SucursalDTO request) throws JsonProcessingException {
		log.info(":: Insert Controller {} ", request);
		// pretty print
		String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(request);
		log.info(":: JSON Request {} ", json);
		return new ResponseEntity<>(programacionEjemploService.insertSucursal(request), HttpStatus.OK);
	}
	

	@PostMapping(value = "/borrar", produces = "application/json")
	public ResponseEntity<Void> deleteSucursal(
			@RequestBody SucursalDTO request) throws JsonProcessingException {
		log.info(":: Delete Controller {} ", request);
		String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(request);
		log.info(":: JSON Request {} ", json);
		programacionEjemploService.deleteSucursal(request);
		return ResponseEntity.ok().build();
	}
	

}
