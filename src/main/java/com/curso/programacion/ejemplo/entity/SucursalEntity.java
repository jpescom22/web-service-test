package com.curso.programacion.ejemplo.entity;

import java.time.Instant;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.GenericGenerator;

@Entity(name = "Sucursal")
public class SucursalEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	private Integer id;

	private String nombre;

	private String direccion;

	@Column(name = "fechaapertura")
	private Instant fechaApertura;

	@OneToMany(mappedBy = "idSucursal", 
			   orphanRemoval = false, 
			   fetch = FetchType.LAZY)
	private List<SucursalEmpleadoEntity> sucursalEmpleadoEntity;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Instant getFechaApertura() {
		return fechaApertura;
	}

	public void setFechaApertura(Instant fechaApertura) {
		this.fechaApertura = fechaApertura;
	}

	public List<SucursalEmpleadoEntity> getSucursalEmpleadoEntity() {
		return sucursalEmpleadoEntity;
	}

	public void setSucursalEmpleadoEntity(List<SucursalEmpleadoEntity> sucursalEmpleadoEntity) {
		this.sucursalEmpleadoEntity = sucursalEmpleadoEntity;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
