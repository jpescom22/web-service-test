package com.curso.programacion.ejemplo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;


@SpringBootApplication
@EnableAutoConfiguration(exclude = RabbitAutoConfiguration.class) 
public class ProgramacionEjemploApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProgramacionEjemploApplication.class, args);
	}

}
