package com.curso.programacion.ejemplo.dto;

import java.time.Instant;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class SucursalDTO {

	private Integer id;

	private String nombre;

	private String direccion;

	private Instant fechaApertura;

	private List<SucursalDTO> sucursalEmpleadoEntity;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Instant getFechaApertura() {
		return fechaApertura;
	}

	public void setFechaApertura(Instant fechaApertura) {
		this.fechaApertura = fechaApertura;
	}

	public List<SucursalDTO> getSucursalEmpleadoEntity() {
		return sucursalEmpleadoEntity;
	}

	public void setSucursalEmpleadoEntity(List<SucursalDTO> sucursalEmpleadoEntity) {
		this.sucursalEmpleadoEntity = sucursalEmpleadoEntity;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
