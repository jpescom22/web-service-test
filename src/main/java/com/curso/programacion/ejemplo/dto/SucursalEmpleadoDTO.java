package com.curso.programacion.ejemplo.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class SucursalEmpleadoDTO {

	private Integer idEmpleado;

	private SucursalDTO idSucursal;

	private String nombre;

	private Integer edad;

	private Boolean active;

	public Integer getIdEmpleado() {
		return idEmpleado;
	}

	public void setIdEmpleado(Integer idEmpleado) {
		this.idEmpleado = idEmpleado;
	}

	public SucursalDTO getIdSucursal() {
		return idSucursal;
	}

	public void setIdSucursal(SucursalDTO idSucursal) {
		this.idSucursal = idSucursal;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
