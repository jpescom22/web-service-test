package com.curso.programacion.ejemplo.service;

import com.curso.programacion.ejemplo.dto.SucursalDTO;

public interface ProgramacionEjemploService {

	SucursalDTO getSucursal(SucursalDTO request);
	
	SucursalDTO updateSucursal(SucursalDTO request);
	
	SucursalDTO insertSucursal(SucursalDTO request);
	
	void deleteSucursal(SucursalDTO request);
	
}
