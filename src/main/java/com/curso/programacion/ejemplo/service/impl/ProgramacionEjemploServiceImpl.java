package com.curso.programacion.ejemplo.service.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.curso.programacion.ejemplo.dto.SucursalDTO;
import com.curso.programacion.ejemplo.entity.SucursalEntity;
import com.curso.programacion.ejemplo.mapper.Mapper;
import com.curso.programacion.ejemplo.repository.SucursalRepository;
import com.curso.programacion.ejemplo.service.ProgramacionEjemploService;

@Service
public class ProgramacionEjemploServiceImpl implements ProgramacionEjemploService {
	
	private static final Logger log = LoggerFactory.getLogger(ProgramacionEjemploServiceImpl.class);
	
	@Autowired
	private SucursalRepository sucursalRepository;
	
	@Autowired
	private Mapper mapper;
	
	@Override
	public SucursalDTO getSucursal(SucursalDTO request) {
		log.info("############ Get Service {} ", request);
		final Optional<SucursalEntity> sucursal = sucursalRepository.findById(request.getId());
		if (sucursal.isPresent()) {
			return mapper.mapSucursalEntityToSucursalDTO(sucursalRepository.findById(request.getId()).get());
		} else {
			return new SucursalDTO();
		}
	}

	@Override
	public SucursalDTO insertSucursal(SucursalDTO request) {
		log.info(":: Insert Service {} ", request);
		return mapper.mapSucursalEntityToSucursalDTO(
				sucursalRepository.save(mapper.mapSucursalDTOToEntityInsert(request)));
	}

	@Override
	public SucursalDTO updateSucursal(SucursalDTO request) {
		log.info(":: Update Service {} ", request);
		return mapper.mapSucursalEntityToSucursalDTO(
				sucursalRepository.save(mapper.mapSucursalDTOToEntityUpdate(request)));
	}

	@Override
	public void deleteSucursal(SucursalDTO request) {
		log.info(":: Delete Service {} ", request);
		sucursalRepository.deleteById(request.getId());
	}



}
