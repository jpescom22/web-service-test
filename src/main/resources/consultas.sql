create database ejemploCurso;
use ejemploCurso;

CREATE TABLE Sucursal(    
	id  int(5) unsigned NOT NULL AUTO_INCREMENT,
    nombre varchar (100) NOT NULL,
	direccion varchar(200) NOT NULL,
	fechaApertura DATE,
	PRIMARY KEY (id)
);

CREATE TABLE Sucursal_Empleado(
	idEmpleado int(10) unsigned NOT NULL AUTO_INCREMENT,
    idSucursal int(5) unsigned NOT NULL,
    nombre varchar (100) NOT NULL,
    edad int(3) NOT NULL,
    active Boolean NOT NULL,
	PRIMARY KEY (idEmpleado),
	FOREIGN KEY (idSucursal) REFERENCES Sucursal(id)
);

######################################################################################

insert into sucursal values(0, 'Sucursal de Acapulco - Costera', 'Costera Miguel Aleman 190, C.P. 99800', CURRENT_DATE());
insert into sucursal values(0, 'Sucursal de Acapulco - Costera', 'Costera Miguel Aleman 190, C.P. 99800', CURRENT_DATE());
commit;

insert into Sucursal_Empleado values(0, 1, 'Adolfo Gutierrez Solis', 40, true);
select * from Sucursal_Empleado;

update sucursal set nombre = 'Sucursal Nueva Acapulco Norte' where id = 2;
commit;

delete from sucursal where id = 2;
commit;

select * from Sucursal;
select * from Sucursal where id = 1;
select * from Sucursal_Empleado;
