'use strict'

const domain = "http://localhost:9091";

function loadDoc(method, path, body) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      const obj = JSON.parse(xhttp.responseText);
      alert(":: JSON (Respuesta desde el servidor): " + xhttp.responseText);
      alert(":: ID (Respuesta desde el servidor): " + obj.id);
    }
  };
  xhttp.open(method, domain + path, true);
  xhttp.setRequestHeader("Content-Type", "application/json");
  xhttp.setRequestHeader("Accept", "application/json");
  xhttp.send(body);
}